﻿using System;

namespace Ref
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            SimpleMethod(ref i);
            Console.WriteLine(i);
        }

        public static void SimpleMethod(ref int j)
        {
            j = 101;
        }
    }
}
