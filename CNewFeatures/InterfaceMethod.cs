﻿using System;

namespace CNewFeatures
{
	interface I_interface
	{
		void Display_1();

		public void Display_2()
		{
			Console.WriteLine("Hello!! Default Method");
		}
	}

	class InterfaceMethod : I_interface
		{

			public void Display_1()
			{
				Console.WriteLine("Hello!! Method");
			}

	
			public static void Main(String[] args)
			{

				InterfaceMethod interfaceObj = new InterfaceMethod();

				interfaceObj.Display_1();

				
				I_interface obj = interfaceObj;

				
				obj.Display_2();
			}
		}
	}




