﻿using System;

namespace NullableReferenceMethod
{
    class Nullable
    {
        static void Main(string[] args)
        {
            ////Nullable Method
            string firstName = "Jake ";
            int? age = null; 
            string lastName = " Archibald";

            Console.WriteLine(firstName + age + lastName); 



            //Stackalloc 

            Span<int> numbers = stackalloc[] { 1, 2, 3, 4, 5, 6 };

            var index = numbers.IndexOfAny(stackalloc[] { 4, 8, 12 });

            Console.WriteLine(index);   
        }


    }
}
