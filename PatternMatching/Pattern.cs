﻿using System;

namespace PatternMatching
{
    class Student
    {
        public string Name { get; set; } = "Pragati Sonnad";
    }

    class Teacher
    {
        public string Name { get; set; } = "Peter";
        public string Specialization { get; set; } = "Information Science";
    }

    class Pattern
    {
        public static void Main(string[] args)
        {
            Student student = new Student();
          
            Teacher teacher = new Teacher();
           
           
            if (student is Student)                        
            {
                Console.WriteLine(student.Name);
            }
            PatterInSwitch(teacher);
        }
        public static void PatterInSwitch(object obj)  
        {
            switch (obj)
            {
                case Teacher teacher:
                    Console.WriteLine(teacher.Name);
                    Console.WriteLine(teacher.Specialization);
                    break;
                default:
                    throw new ArgumentException(
                        message: "Oject is not recognized",
                        paramName: nameof(obj));
            }
        }
    }
}
